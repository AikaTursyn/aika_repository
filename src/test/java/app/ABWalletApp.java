package app;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.MalformedURLException;
import java.net.URL;

public class ABWalletApp {
    public static void main (String [] args) throws MalformedURLException {


        DesiredCapabilities capabilities = new DesiredCapabilities();

        capabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.associatedbankwallet");
        capabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.associatedbankwallet.MainActivity");
        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Emulator");
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "android");
        AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),capabilities);


//        Thread.sleep(10000);
//        driver.findElement(By.xpath("//android.widget.TextView[@text='Sam Cardholder']")).click();
//
//        WebDriverWait wait = new WebDriverWait(driver, 2);
//        AndroidElement androidElement = driver.findElement(By.id("homeCardID_card1"));
//        if(androidElement.isDisplayed()) androidElement.click();
//
//        driver.pressKeyCode(AndroidKeyCode.BACK);
        WebDriverWait wait = new WebDriverWait(driver, 2);

        AndroidElement androidElement= driver.findElement(By.xpath("//android.view.ViewGroup[@content-desc='homeCardID_card1']"));
        if(androidElement.isDisplayed()) androidElement.click();
    }
}


