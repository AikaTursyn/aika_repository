package utilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.AndroidMobileCapabilityType;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class Driver {

    private Driver(){

    }

    private static DesiredCapabilities desiredCapabilities;

    public static DesiredCapabilities getDriverReference() {
        return desiredCapabilities;

    }

    public static DesiredCapabilities getDriver() throws MalformedURLException {

        desiredCapabilities.setCapability(AndroidMobileCapabilityType.APP_PACKAGE, "com.associatedbankwallet");
        desiredCapabilities.setCapability(AndroidMobileCapabilityType.APP_ACTIVITY, "com.associatedbankwallet.MainActivity");
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Emulator");
        desiredCapabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "android");
      AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(new URL("http://127.0.0.1:4723/wd/hub"),getDriver());

        return driver;


    }


}
